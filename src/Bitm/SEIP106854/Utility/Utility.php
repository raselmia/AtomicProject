<?php


namespace Rasel\BITM\SEIP106854\Utility;

class Utility{
    
    
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/AtomicProject/Views/SEIP106854/TvModel/index.php"){
        header("Location:".$url);
    }
    static public function redirect1($url="/AtomicProject/Views/SEIP106854/Date/index.php"){
        header("Location:".$url);
    }
    static public function redirect2($url="/AtomicProject/Views/SEIP106854/TextSummary/index.php"){
        header("Location:".$url);
    }
     static public function redirect3($url="/AtomicProject/Views/SEIP106854/Gender/index.php"){
        header("Location:".$url);
    }
       static public function redirect4($url="/AtomicProject/Views/SEIP106854/File/index.php"){
        header("Location:".$url);
    }
      static public function redirect5($url="/AtomicProject/Views/SEIP106854/City/index.php"){
        header("Location:".$url);
    }
        static public function redirect6($url="/AtomicProject/Views/SEIP106854/Subscribe/index.php"){
        header("Location:".$url);
    }
}