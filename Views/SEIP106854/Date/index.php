

<?php
    ini_set('display_errors','Off');
    include_once("../../../vendor/autoload.php");
    use Rasel\Bitm\SEIP106854\Date\Birthday;
    
    $date = new Birthday();
    $birthdays = $date->index();
    
    
    
?>






<html>
    <head>
        <title>Birthday-list</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
         <section>
<?php

include_once "../../../page/header.php";
?>
      </section>
    <div class="container">


        <h1>List of Birthday</h1>
        <div>
       <a href="create.php">Add</a> 
       <p style="float:right">DownLoad as PDF | XL</p>
       </div>
        <table class=" table table-bordered" >
        <thead>
            <tr>
            <td> Sl no</td>
                <td>Name</td>
                <td> Birthdate</td>  
                  
                  <td>Action</td>
            
            </tr>
            </thead>
            <tbody>
            <?php
            $slno=1;
            foreach($birthdays as $date){


            ?>
            
              <tr>
              <td><?php echo $slno;?></td>
                  <td><?php echo $date->name;?></td>
                <td><?php echo $date->date;?></td>
                   
                <td><a href="store.php">View |</a>&nbsp;<a href="update.php">Edit |</a>&nbsp;

                <a href="Delete.php">Delete |</a>&nbsp<a href="recover.php">Trash/Recover |</a>&nbsp<a href="#">Email to Friend</a></td>
                  
                  
                  
            

            
            </tr>
            <?php
            $slno++;
          }

            ?>


            </tbody>
        </table>
        </div>
 </body>
        <ul class="pagination">
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>
      
        
    
      <section>
<?php
include_once "../../../page/footer.php";
?>
</section>
</html>





